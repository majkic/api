# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This project is my playground with a lot of useful code I wanted to share with others
* Version 1.0.0
* To learn more about me please visit [my portfolio site](https://majkic.in.rs)

### How do I get set up? ###

* No special setup, just plug and play

### Contribution guidelines ###

* Writing tests - I did most of the development using TDD - which was really fun for me and resulted in better written code
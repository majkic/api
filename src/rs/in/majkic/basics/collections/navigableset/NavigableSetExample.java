package rs.in.majkic.basics.collections.navigableset;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * Created by majkic on 10/20/17.
 */
public class NavigableSetExample {
    public int example(){
        List<Integer> l = new ArrayList<>();
        l.add(44);
        l.add(65);
        NavigableSet<Integer> n = new TreeSet<>();
        n.add(1);
        n.add(6);
        n.add(13);
        n.add(18);
        System.out.println(n.ceiling(7));
        System.out.println(n.floor(7));
        System.out.println(n.lower(7));
        System.out.println(n.last());
        System.out.println(n.first());
        System.out.println(n.addAll(l));
        System.out.println(n.size());
        System.out.println(n.pollFirst());
        System.out.println(n.pollLast());
        System.out.println(n.size());
        return 0;
    }
}

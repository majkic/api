package rs.in.majkic.basics.collections.navigableset;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/20/17.
 */
public class NavigableSetExampleTest {
    private NavigableSetExample navigableSetExample;
    @Before
    public void setUp() throws Exception {
        navigableSetExample = new NavigableSetExample();
    }

    @After
    public void tearDown() throws Exception {
        navigableSetExample = null;
    }

    @Test
    public void testExample() throws Exception {
        assertEquals(0, navigableSetExample.example());
    }
}
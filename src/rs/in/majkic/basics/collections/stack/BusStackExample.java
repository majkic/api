package rs.in.majkic.basics.collections.stack;

import java.util.Stack;

/**
 * Created by majkic on 10/13/17.
 */
public class BusStackExample {
    public int search(int searchTerm, int[] people){
        Stack stack = new Stack();
        for (int i=0;i<people.length;i++){
            stack.push(people[i]);
        }
        int index = stack.search(searchTerm);
        return index;
    }

    /*
    Remove every step element from Stack
     */
    public int removeElements(int step, int[] elements){
        Stack stack = new Stack();
        int counter = 0;
        for (int i=0;i<elements.length;i++) {
            if ((i + 1) % step != 0) {
                stack.push(elements[i]);
            }
        }
        return stack.size();
    }

    public void printStack(){
        Stack<String> st = new Stack();
        st.add("A");
        st.add("B");
        st.add("C");
        for(String s : st){
            System.out.println(s);
        }
        //returns current capacity of Stack
        System.out.println(st.capacity());
        //returns true if empty
        System.out.println(st.empty());
        //returns first element
        System.out.println(st.firstElement());
        //returns last element
        System.out.println(st.lastElement());
        //gets the top element but does not remove
        System.out.println(st.peek());
        //pushes the element at the top of element
        System.out.println(st.push("D"));
        //finds the first index for D
        System.out.println(st.search("D"));
        //removes the element from the top
        System.out.println(st.pop());
        //finds the first index for B
        System.out.println(st.search("B"));
        //prints size of stack
        System.out.println(st.size());

    }
}

package rs.in.majkic.basics.collections.stack;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/13/17.
 */
public class BusStackExampleTest {
    private BusStackExample busStackExample;
    @Before
    public void setUp() throws Exception {
        busStackExample = new BusStackExample();
    }

    @After
    public void tearDown() throws Exception {
        busStackExample = null;
    }

    @Test
    public void testExecute() throws Exception {
        busStackExample.printStack();
        assertEquals(3, busStackExample.search( 23, new int[] {12, 13, 23, 44, 84}));
    }
}
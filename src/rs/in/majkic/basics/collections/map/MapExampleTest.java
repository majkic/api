package rs.in.majkic.basics.collections.map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/18/17.
 */
public class MapExampleTest {
    private MapExample mapExample;
    @Before
    public void setUp() throws Exception {
        mapExample = new MapExample();
    }

    @After
    public void tearDown() throws Exception {
        mapExample = null;
    }

    @Test
    public void testExample() throws Exception {
        assertEquals(0, mapExample.example());
    }
}
package rs.in.majkic.basics.collections.map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by majkic on 10/18/17.
 */
public class MapExample {
    public int example(){
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "A");
        map.put(2, "B");
        map.put(3, "C");
        System.out.println(map.containsKey(1));
        System.out.println(map.get(2));
        map.put(4, "D");
        System.out.println(map.size());
        System.out.println(map.remove(2));
        System.out.println(map.containsKey(2));
        System.out.println(map.replace(1, "AA"));
        System.out.println(map.get(1));
        System.out.println(map);
        return 0;
    }
}

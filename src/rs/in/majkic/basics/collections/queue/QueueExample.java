package rs.in.majkic.basics.collections.queue;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by majkic on 10/18/17.
 */
public class QueueExample {
    public int example(){
        Queue<String> q = new ArrayDeque<>();//Double Ended Queue
        q.add("A");
        q.add("B");
        q.add("C");
        System.out.println(q.element());
        System.out.println(q.peek());
        System.out.println(q.offer("D"));
        System.out.println(q.poll());
        System.out.println(q.remove());
        System.out.println(q.element());
        System.out.println(q.size());
        System.out.println(q.poll());
        System.out.println(q.element());
        System.out.println(q.size());
        return 0;
    }
}

package rs.in.majkic.basics.collections.queue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/18/17.
 */
public class QueueExampleTest {
    private QueueExample queueExample;
    @Before
    public void setUp() throws Exception {
        queueExample = new QueueExample();
    }

    @After
    public void tearDown() throws Exception {
        queueExample = null;
    }

    @Test
    public void testExample() throws Exception {
        assertEquals(0, queueExample.example());
    }
}
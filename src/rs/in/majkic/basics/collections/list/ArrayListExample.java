package rs.in.majkic.basics.collections.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by majkic on 10/19/17.
 */
public class ArrayListExample {
    public int example(){
        List<String> s = new ArrayList<>();
        s.add("A");
        s.add("B");
        s.add("C");
        System.out.println(s.size());
        System.out.println(s.remove(1));
        System.out.println(s);
        System.out.println(s.contains("A"));
        System.out.println(s.get(1));
        System.out.println(s.indexOf("A"));
        System.out.println(s.add("D"));
        System.out.println(s.add("E"));
        //System.out.println(s.set(2, "E"));
        System.out.println(s);
        return 0;
    }

    public int sortExample(){
        List<String> s = new ArrayList<>();
        s.add("A");
        s.add("B");
        s.add("C");
        s.add("A");
        s.add("B");
        s.add("C");
        s.add("A");
        s.add("B");
        s.add("C");
        System.out.println(s);
        Collections.sort(s);
        System.out.println(s);
        return 0;
    }
}

package rs.in.majkic.basics.collections.list;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/24/17.
 */
public class LinkedListExampleTest {
    public LinkedListExample linkedListExample;
    @Before
    public void setUp() throws Exception {
        linkedListExample = new LinkedListExample();
    }

    @After
    public void tearDown() throws Exception {
        linkedListExample = null;
    }

    @Test
    public void testExample() throws Exception {
        assertEquals(0, linkedListExample.example());
    }
}
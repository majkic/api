package rs.in.majkic.basics.collections.list;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/19/17.
 */
public class ArrayListExampleTest {
    private ArrayListExample arrayListExample;
    @Before
    public void setUp() throws Exception {
        arrayListExample = new ArrayListExample();
    }

    @After
    public void tearDown() throws Exception {
        arrayListExample = null;
    }

    @Test
    public void testExample() throws Exception {
        assertEquals(0, arrayListExample.example());
    }

    @Test
    public void testSortExample() throws Exception {
        assertEquals(0, arrayListExample.sortExample());
    }
}
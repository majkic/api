package rs.in.majkic.basics.collections.list;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by majkic on 10/24/17.
 */
public class LinkedListExample {
    public int example(){
        List<String> l = new LinkedList<>();
        l.add("A");
        l.add("B");
        l.add("C");
        System.out.println(l.get(1));
        l.remove(0);
        System.out.println(l);
        l.add("A");
        System.out.println(l);
        l.addAll(l);
        System.out.println(l);
        List<String> ll = new LinkedList<>();
        for (String a:l){
            System.out.print(a);
            ll.add(a);
        }
        System.out.println();
        Collections.sort(ll);
        l.addAll(ll);
        System.out.println(l);
        System.out.println(generateHugeList(10));
        return 0;
    }

    private List<String> generateHugeList(int n){
        List<String> l = new LinkedList<>();
        //org.apache.commons.lang.RandomStringUtils would be an alternative

        for (int i=0;i<n;i++){
            Random random = new Random();
            int a = random.nextInt(65) + 57;

            char c = (char) a;
            l.add(String.valueOf(c));
        }
        return l;
    }
}

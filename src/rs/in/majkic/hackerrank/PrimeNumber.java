package rs.in.majkic.hackerrank;

/**
 * Created by majkic on 5/3/16.
 */
public class PrimeNumber {
    public void checkPrime(int ...arr) {

        for(int i=0;i<arr.length;i++)
        {
            int num = arr[i];

            //REMEMBER: 1 is not a Prime Number
            if(isPrime(num) && num != 1) {

                System.out.print(num+ " ");

            }
        }

        System.out.print("\n");
    }

    //This method isPrime() will determine if a given number is either prime or not.
    public  boolean isPrime( int num )
    {
        boolean primality = true;

        //Sets and stores the "upper boundary divisor" as the √n in the variable "limit" of type integer.
        int limit = (int) Math.sqrt ( num );

        //Our boundary is set with this range now
        for ( int i = 2; i <= limit; i++ )
        {
            //Divides variable "num" by the range of divisors set above to resolve "primality"
            if ( num % i == 0 )
            {
                //primality is true
                primality = false;
                break;
            }
        }
        //primality is true
        return primality;
    }

    public void checkPrimeMy(int ...arr){
        for (int j = 0; j < arr.length; j++){
            int n = arr[j];
            boolean prime = true;
            for (int i=2; i<=((int) Math.sqrt(n));i++){
                if (n%i == 0){
                    prime = false;
                    break;
                }
            }
            if (n == 1) prime=false;
            if (prime){
                System.out.print(n + " ");
            } else {
                //System.out.print(" ");
            }
        }
        System.out.println();
    }
}

package rs.in.majkic.hackerrank;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by majkic on 5/18/16.
 */
public class ModifiedFibonacci {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int A = sc.nextInt();
        int B = sc.nextInt();
        int N = sc.nextInt();
        System.out.println(calculate(A, B, N));
    }

    public static BigInteger calculate(int A, int B, int N){
        BigInteger s = BigInteger.valueOf(0);
        BigInteger newA = BigInteger.valueOf(A);
        BigInteger newB = BigInteger.valueOf(B);
        if (N == 0){
            return newA;
        } else if (N == 1){
            return newB;
        } else {
            for (int i=2;i<N;i++){
                s = newA.add(newB.pow(2));
                newA = newB;
                newB = s;
            }
            return s;
        }
    }
}

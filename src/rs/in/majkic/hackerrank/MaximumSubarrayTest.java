package rs.in.majkic.hackerrank;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by majkic on 5/12/16.
 */

public class MaximumSubarrayTest {
    @Test
    public void maxSumSubarray(){
        int[] a = {2, -1, 2, 3, 4, -5};
        assertEquals(10, MaximumSubarray.maxSumSubarray(a));
    }
}

package rs.in.majkic.hackerrank;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by majkic on 5/4/16.
 */
public class MyCalculatorTest {
    @Test
    public void divisorSumTest(){
        MyCalculator myCalc = new MyCalculator();
        assertEquals(12, myCalc.divisorSum(6));
    }
}

package rs.in.majkic.hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by majkic on 5/30/16.
 */
public class RegExPatterns {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        List<String> names = new ArrayList<>();
        for(int a0 = 0; a0 < N; a0++){
            String firstName = in.next();
            String emailID = in.next();
            if (emailID.endsWith("@gmail.com")){
                names.add(firstName);
            }
        }

        Collections.sort(names);
        for(String name:names){
            System.out.println(name);
        }
    }

}

package rs.in.majkic.hackerrank;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by majkic on 5/26/16.
 */
public class BinaryTreeTraversal {

    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }
        levelOrder(root);
    }

    /**
     * Level-Order Traversal
     * Input:6 3 5 4 7 2 1
     * Output: 4 2 6 1 3 5 7
     * @param root
     */
    static void levelOrder(Node root){
        //Write your code here
        Queue<Node> q = new LinkedList<>();
        if (root != null) {
            q.add(root);
        }
        visit(q);
        System.out.println();
    }

    static void visit(Queue<Node> q) {
        int startingQueueSize = q.size();

        for (int i = 1; i <= startingQueueSize; i++) {
            Node n = q.remove();
            System.out.print(n.data + " ");
            if (n.left != null)
                q.add(n.left);
            if (n.right != null)
                q.add(n.right);
        }

        if (q.size() > 0){
            visit(q);
        }
    }

    /**
     * In-Order Traversal
     * Input:6 3 5 4 7 2 1
     * Output: 1 2 3 4 5 6 7
     * @param root
     */
    static void inOrder(Node root){

    }

    /**
     * Post-Order Traversal
     * Input:6 3 5 4 7 2 1
     * Output: 1 3 2 5 7 6 4
     * @param root
     */
    static void postOrder(Node root){

    }

    /**
     * Pre-Order Traversal
     * Input:6 3 5 4 7 2 1
     * Output: 4 2 1 3 6 5 7
     * @param root
     */
    static void preOrder(Node root){

    }

    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
}

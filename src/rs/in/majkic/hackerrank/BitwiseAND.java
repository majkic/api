package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/31/16.
 */
public class BitwiseAND {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            int k = in.nextInt();
            int max = 0;
            int maxa = 0;
            int maxb = 0;
            for (int i=0;i<n+1;i++){
                for(int j=i+1;j<n+1;j++){
                    int num = i & j;
                    if((num < k) && (num > max)){
                        max = num;
                        maxa = i;
                        maxb = j;
                    }
                }
            }
            System.out.println(max);
        }
    }
}

package rs.in.majkic.hackerrank;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import static java.util.Arrays.*;

/**
 * Created by majkic on 5/4/16.
 */
public class BasicBigDecimal {

    public static void main(String []argh)
    {
        //Input
        Scanner sc= new Scanner(System.in);
        int n=sc.nextInt();
        String []s=new String[n+2];
        for(int i=0;i<n;i++)
        {
            s[i]=sc.next();
        }
        //Write your code here
        BigDecimal []bi = new BigDecimal[n];
        int i;
        for (i=0;i<n;i++){
            bi[i] = new BigDecimal(s[i]);
        }
        //sort bi and s as well
        int j;
        boolean flag = true;   // set flag to true to begin first pass
        BigDecimal temp;   //holding variable
        String tempString;

        while ( flag )
        {
            flag= false;    //set flag to false awaiting a possible swap
            for( j=0;  j < bi.length -1;  j++ )
            {
                if ( bi[ j ].compareTo(bi[j+1]) == -1 )   // change to > for ascending sort
                {
                    temp = bi[ j ];                //swap elements
                    bi[ j ] = bi[ j+1 ];
                    bi[ j+1 ] = temp;
                    flag = true;              //shows a swap occurred

                    tempString = s[ j ];
                    s[ j ] = s[ j+1 ];
                    s[j+1] = tempString;
                }
            }
        }
        // sort end
         //Output
        for(i=0;i<n;i++)
        {
            System.out.println(s[i]);
        }

    }
}

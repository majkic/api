package rs.in.majkic.hackerrank;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by majkic on 5/28/16.
 */
public class DateCalculator {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int da = sc.nextInt();
        int ma = sc.nextInt();
        int ya = sc.nextInt();
        sc.nextLine();
        int de = sc.nextInt();
        int me = sc.nextInt();
        int ye = sc.nextInt();
        if (ye>ya){
            System.out.println(0);
        } else if (ye<ya){
            System.out.println(10000);
        } else {//ye==ya
            if (me>ma){
                System.out.println(0);
            } else if (me<ma){
                System.out.println((ma-me)*500);
            } else {//me==ma
                if (de>=da){
                    System.out.println(0);
                } else {
                    System.out.println((da-de)*15);
                }
            }
        }

        /*
        Date d1 = setupDate(da+"-"+ma+"-"+ya);
        Date d2 = setupDate(de+"-"+me+"-"+ye);
        long daysDiff = daysDiff(d2, d1);
        */
    }

    private static Date setupDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    private static long daysDiff(Date d1, Date d2){
        long diff = d2.getTime() - d1.getTime();
        return (diff / (24 * 60 * 60 * 1000));
    }

    private static long calcMoney(long days){
        if (days<=0){
            return 0;
        } else {
            return days*100;
        }
    }
}

package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/12/16.
 */
public class MaximumSubarray {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        sc.nextLine();
        int i = 0;
        int n = 0;
        int j = 0;
        int []maxSumSubarray = new int[t];
        int []maxSum = new int[t];
        for (i=0;i<t;i++){
            n = sc.nextInt();
            sc.nextLine();
            int []arr = new int[n];
            for (j=0;j<n;j++){
                arr[j] = sc.nextInt();
            }
            maxSumSubarray[i] = maxSumSubarray(arr);
            maxSum[i] = maxSum(arr);
        }
        for (i=0;i<t;i++){
            System.out.println(maxSumSubarray[i] + " " + maxSum[i]);
        }
    }

    public static int maxSumSubarray(int []a){
        int max = 0;
        for (int x=0;x<a.length;x++){
            int sum = 0;
            for (int y=x;y<a.length;y++){
                sum = arrSum(getSubArray(a,x,y));
                if (sum>max) max = sum;
            }
        }
        return max;
    }

    private static int maxSum(int []a){
        int sum=0;
        for (int x=0;x<a.length;x++){
            if(a[x]>0){
                sum += a[x];
            }
        }
        return sum;
    }

    public static int arrSum(int[] arr){
        int sum = 0;
        for (int i=0;i<arr.length;i++){
            sum += arr[i];
        }
        return sum;
    }

    public static int[] getSubArray(int[] arr, int start, int end){
        int []tempA = new int[end-start+1];
        int ind = 0;
        for (int x=start;x<end+1;x++) {
            tempA[ind] = arr[x];
            ind++;
        }
        //System.out.println("Subarray: " + Arrays.toString(tempA));
        return tempA;
    }
}

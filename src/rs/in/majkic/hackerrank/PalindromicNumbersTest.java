package rs.in.majkic.hackerrank;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 5/21/16.
 */
public class PalindromicNumbersTest {

    @Test
    public void swapPalindromesTest(){
        assertEquals("3092", PalindromicNumbers.swapPalindromes("2903", 1));
        assertEquals("3093442", PalindromicNumbers.swapPalindromes("2093443", 0));
    }

}
package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/21/16.
 */
public class PalindromicNumbers {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String n = input.split(" ")[0];
        int k = Integer.valueOf(input.split(" ")[1]);
        String out = findMaxPalindrome(n, k);
        System.out.println(String.valueOf(out));
    }

    public static String findMaxPalindrome(String n, int k) {
        String o = "";
        int numOfChanges = 0;
        int position = 0;
        while (numOfChanges <= k && position<=Integer.valueOf(n)/2){
            n = swapPalindromes(n, position);
            numOfChanges++;
            position++;
        }
        return n;
    }

    public static String swapPalindromes(String n, int position) {
        String firstString = n.substring(position, position+1);
        String mirrorString = n.substring(n.length()-position-1, n.length()-position);
        String out = n;
        if (Integer.valueOf(firstString) < Integer.valueOf(mirrorString)) {

            String a = n.substring(0, position);
            String b = "";
            if (position + 1 <= n.length() - position - 2) {
                b = n.substring(position + 1, n.length() - position - 1);
            }

            String c = n.substring(n.length() - position, n.length());
            out = a + mirrorString + b + firstString + c;
        }
        return  out;
    }
}

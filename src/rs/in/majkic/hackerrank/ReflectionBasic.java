package rs.in.majkic.hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by majkic on 5/4/16.
 */
public class ReflectionBasic {
    public static void main(String[] args) {
        Do_Not_Terminate.forbidExit();

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int num = 8;//Integer.parseInt(br.readLine().trim());
            Object o;// Must be used to hold the reference of the instance of the class Solution.Inner.Private
//My code
            ReflectionBasic.Inner nestedObject = new ReflectionBasic.Inner();
            o = nestedObject.new Private();
            Class priv = o.getClass();

            Class[] types = { int.class };
            Method m = priv.getDeclaredMethod("powerof2", types);
            m.setAccessible(true);
            System.out.println("method = " + m.toString());
            String ret = (String) m.invoke(o, num);
            System.out.println(num + " is " + ret);
            System.out.println("An instance of class: "+o.getClass().getCanonicalName()+" has been created");

        }//end of try

        catch (Do_Not_Terminate.ExitTrappedException e) {
            System.out.println("Unsuccessful Termination!!");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }//end of main
    static class Inner
    {
        private class Private{
            private String powerof2(int num)
            {
                return ((num&num-1)==0)?"power of 2":"not a power of 2";
            }
        }
    }//end of Inner

//end of ReflectionBasic


    public static void studentReflection(){
        Class student = (new Student()).getClass();
        Method[] methods = student.getDeclaredMethods();

        ArrayList<String> methodList = new ArrayList<>();
        for(Method method: methods){
            methodList.add(method.getName());
        }
        Collections.sort(methodList);
        for(String name: methodList){
            System.out.println(name);
        }
    }
}

class Do_Not_Terminate {

    public static class ExitTrappedException extends SecurityException {

        private static final long serialVersionUID = 1L;
    }

    public static void forbidExit() {
        final SecurityManager securityManager = new SecurityManager() {
            @Override
            public void checkPermission(Permission permission) {
                if (permission.getName().contains("exitVM")) {
                    throw new ExitTrappedException();
                }
            }
        };
        System.setSecurityManager(securityManager);
    }
}

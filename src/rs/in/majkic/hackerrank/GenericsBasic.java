package rs.in.majkic.hackerrank;

import com.sun.tools.javac.jvm.Gen;

import static java.lang.Math.E;

/**
 * Created by majkic on 5/23/16.
 */
public class GenericsBasic {
    public static void main(String[] args){
        Integer[] intArray = { 1, 2, 3 };
        String[] stringArray = { "Hello", "World" };

        printArray( intArray  );
        printArray( stringArray );

        if(GenericsBasic.class.getDeclaredMethods().length > 2){
            System.out.println("You should only have 1 method named printArray.");
        }
    }

    private static <E> void printArray(E[] elementArray){
        for(E element:elementArray){
            System.out.println(element);
        }
    }
}

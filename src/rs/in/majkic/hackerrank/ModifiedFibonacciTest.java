package rs.in.majkic.hackerrank;

import org.junit.Test;

import java.math.BigInteger;

import static junit.framework.Assert.assertEquals;

/**
 * Created by majkic on 5/18/16.
 */
public class ModifiedFibonacciTest {
    @Test
    public void calculate() throws Exception {
        assertEquals(BigInteger.valueOf(5), ModifiedFibonacci.calculate(0, 1, 5));
        assertEquals(new BigInteger("84266613096281243382112"), ModifiedFibonacci.calculate(0, 1, 10));
    }

}

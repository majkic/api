package rs.in.majkic.hackerrank;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class SubStringManager {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        int subSize = in.nextInt();
        String subMin = input.substring(0,subSize);
        String subMax = input.substring(0,subSize);
        String tempSub = "";
        for (int i=1; i<input.length()-subSize+1; i++){
            tempSub = input.substring(i, i+subSize);
            subMin = (subMin.compareTo(tempSub) > 0)?tempSub:subMin;
            subMax = (subMax.compareTo(tempSub) > 0)?subMax:tempSub;
        }
        System.out.println(subMin);
        System.out.println(subMax);
    }
}
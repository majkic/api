package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/5/16.
 */
public class ArrayBasic {
    public static void main(String[] args){
        Scanner sc = new Scanner (System.in);
        int n = sc.nextInt();
        sc.nextLine();
        int []arr = new int[n];
        int i, j, k;
        for(k=0;k<n;k++){
            arr[k] = sc.nextInt();
        }

        int []tempArr;
        int negativeArrayCounter = 0;
        for(i=0;i<n;i++){
            for (j=i;j<n;j++){
                tempArr = (new ArrayBasic()).getSubArray(arr, i, j);
                if ((new ArrayBasic()).arrSum(tempArr)<0){
                    negativeArrayCounter++;
                }
            }
        }
        System.out.println(negativeArrayCounter);
    }
    public int arrSum(int[] arr){
        int sum = 0;
        for (int i=0;i<arr.length;i++){
            sum += arr[i];
        }
        return sum;
    }

    public int[] getSubArray(int[] arr, int start, int end){
        int []tempA = new int[end-start+1];
        int ind = 0;
        for (int x=start;x<end+1;x++) {
            tempA[ind] = arr[x];
            ind++;
        }
        //System.out.println("Subarray: " + Arrays.toString(tempA));
        return tempA;
    }
}

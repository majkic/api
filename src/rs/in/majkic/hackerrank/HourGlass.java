package rs.in.majkic.hackerrank;

/**
 * Created by majkic on 5/3/16.
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class HourGlass {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arr[][] = new int[6][6];
        for(int i=0; i < 6; i++){
            for(int j=0; j < 6; j++){
                arr[i][j] = in.nextInt();
            }
        }
        int sum = 0;
        int max = 0;
        for(int i=0; i < 4; i++){
            for(int j=0; j < 4; j++){
                //if (isHourGlass(i,j,arr)){
                sum = calculateSum(i,j,arr);
                if (sum>max) max = sum;
                //}
            }
        }
        System.out.println(max);
    }
    @Deprecated
    private static boolean isHourGlass(int x, int y, int [][]arr){
        if ((arr[x][y] == arr[x+2][y]) && (arr[x][y+1] == arr[x+2][y+1]) && (arr[x][y+2] == arr[x+2][y+2]) &&
                (arr[x+1][y] == 0) && (arr[x+1][y+2] == 0)){
            return true;
        } else {
            return false;
        }
    }

    private static int calculateSum (int x, int y, int [][]arr){
        return (arr[x][y] + arr[x][y+1] + arr[x][y+2] + arr[x+1][y+1] + arr[x+2][y] + arr[x+2][y+1] + arr[x+2][y+2]);
    }
}


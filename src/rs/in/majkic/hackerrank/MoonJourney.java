package rs.in.majkic.hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by majkic on 5/19/16.
 */
public class MoonJourney {
    public static void main(String[] args) throws Exception{
        BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
        String[] temp = bfr.readLine().split(" ");
        int N = Integer.parseInt(temp[0]);
        int I = Integer.parseInt(temp[1]);
        HashMap<Integer, Set<Integer>> countries = new HashMap<>();

        for(int i = 0; i < I; i++){
            temp = bfr.readLine().split(" ");
            int a = Integer.parseInt(temp[0]);
            int b = Integer.parseInt(temp[1]);
            countries = checkPair(countries, a, b);
        }
        countries = mergeSets(countries);

        long combinations = 0;
        combinations = calculateCombinations(countries);
        // Compute the final answer - the number of combinations

        System.out.println(combinations);

    }

    private static HashMap<Integer, Set<Integer>> checkPair (HashMap<Integer, Set<Integer>> c, int a, int b){
        HashMap<Integer, Set<Integer>> countries = c;
        if (countries.size() == 0){
            Set<Integer> s = new HashSet<>();
            s.add(a);
            s.add(b);
            countries.put(0, s);
            return countries;
        }
        boolean flag = false;
        for (Map.Entry<Integer, Set<Integer>> element:countries.entrySet()){
            Set<Integer> set = element.getValue();
            if (set.contains(a)){
                flag = true;
                if (set.contains(b)){
                    //do nothing
                } else {
                    set.add(b);
                }
            } else {
                if (set.contains(b)){
                    set.add(a);
                    flag = true;
                }
            }

            if (flag){
                //change hashmap
                countries.put(element.getKey(), set);
                continue;
            }
        }
        if (!flag){
            Set<Integer> s = new HashSet<>();
            s.add(a);
            s.add(b);
            countries.put(countries.size(), s);
        }
        return countries;
    }

    private static int calculateCombinations(HashMap<Integer, Set<Integer>> c){
        int product = 1;
        for (Map.Entry<Integer, Set<Integer>> element:c.entrySet()){
            int membersNo = element.getValue().size();
            product = product*membersNo;
        }
        return product;
    }

    private static HashMap<Integer, Set<Integer>> mergeSets(HashMap<Integer, Set<Integer>> countries){
        //should pass whole HashMap and merge necessary sets
        HashMap<Integer, Set<Integer>> newCountries = countries;
        for (int i=0;i<newCountries.size();i++) {
            Set<Integer> set = newCountries.get(i);
            for (int j=i;j<newCountries.size();j++) {
                Set<Integer> set1 = newCountries.get(j);
                if(overlappingSets(set, set1)){
                    set.addAll(set1);
                    newCountries.remove(j);
                }
            }
        }
        return newCountries;
    }

    private static boolean overlappingSets(Set<Integer> set, Set<Integer> set1){
        Set<Integer> bigSet = set;
        bigSet.addAll(set1);
        if (bigSet.size() != (set.size() + set1.size())){
            return true;
        }
        return false;
    }
}

package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/12/16.
 */
public class BinaryConverter {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(consecutiveOnes(toBinary(n)));
    }

    private static String toBinary (int n){
        String bin = "";
        while (n != 1){
            bin = String.valueOf(n % 2) + bin;
            n = n / 2;
        }
        bin = "1" + bin;
//        System.out.println(n + " - " + bin);
        return bin;
    }

    private static int consecutiveOnes(String bin){
        int count = 0;
        int max = 0;
        String s;
        for (int i=0;i<bin.length();i++){
            s = bin.substring(i,i+1);
            if (s.equals("1")){
                count++;
            } else {
                if (count > max) {
                    max = count;
                }
                count = 0;
            }
            //System.out.println(s + " - " + count);

        }
        if (count > max) {
            max = count;
        }
        return max;
    }
}

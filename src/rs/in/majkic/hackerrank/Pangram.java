package rs.in.majkic.hackerrank;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by majkic on 6/16/16.
 */
public class Pangram {
    public static void main (String[] args){
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        s = s.replaceAll("[^a-zA-Z]", "").toLowerCase();
        String[] array = s.split("");
        Set<String> chSet = new HashSet<String>(Arrays.asList(array));
        chSet.remove("");
        System.out.println(chSet);
        if (chSet.contains(" ")){
            chSet.remove(" ");
        }
        if (chSet.contains(",")){
            chSet.remove(",");
        }
        if (chSet.contains(";")){
            chSet.remove(";");
        }
        if (chSet.contains(".")){
            chSet.remove(".");
        }
        System.out.println(chSet.size());
        if (chSet.size() == 26){
            System.out.println("pangram");
        } else {
            System.out.println("not pangram");
        }
    }

    private static Set populateCharSet() {
        char a;
        Set<Character> chars = new HashSet<>();
        for (int i=97; i<123; i++){
            chars.add((char) i);
        }
        return chars;
    }


}

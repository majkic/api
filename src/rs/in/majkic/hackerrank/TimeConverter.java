package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/12/16.
 */
public class TimeConverter {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String time = in.next();
        String hour24 = time.substring(0,8);
        System.out.println(hour24);
        String part = time.substring(8,10);
        System.out.println(part);
        String hours = time.substring(0, 2);
        if (part.equals("PM")) {
            if (hours.equals("00")) {
                hours = "12";
            } else if (hours.equals("12")) {
                //do nothing
            } else {
                hours = String.valueOf(Integer.valueOf(hours) + 12);
            }
            System.out.println(hours);
            hour24 = hours + hour24.substring(2, 8);
        } else {
            if (hours.equals("12")){
                hours = "00";
                hour24 = hours + hour24.substring(2, 8);
            }
        }
        System.out.println(hour24);
    }
}

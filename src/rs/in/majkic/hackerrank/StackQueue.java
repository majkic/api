package rs.in.majkic.hackerrank;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by majkic on 5/20/16.
 */
public class StackQueue {
    private LinkedList queue = new LinkedList();
    private Stack stack = new Stack();

    private void pushCharacter(char c) {
        stack.push(c);
    }

    private void enqueueCharacter(char c) {
        this.queue.addLast(c);
    }

    private char popCharacter() {
        return (char) stack.pop();
    }

    private char dequeueCharacter() {
        return (char) queue.pop();
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        scan.close();

        // Convert input String to an array of characters:
        char[] s = input.toCharArray();

        // Create a Solution object:
        StackQueue p = new StackQueue();

        // Enqueue/Push all chars to their respective data structures:
        for (char c : s) {
            p.pushCharacter(c);
            p.enqueueCharacter(c);
        }

        // Pop/Dequeue the chars at the head of both data structures and compare them:
        boolean isPalindrome = true;
        for (int i = 0; i < s.length/2; i++) {
            if (p.popCharacter() != p.dequeueCharacter()) {
                isPalindrome = false;
                break;
            }
        }

        //Finally, print whether string s is palindrome or not.
        System.out.println( "The word, " + input + ", is "
                + ( (!isPalindrome) ? "not a palindrome." : "a palindrome." ) );
    }
}

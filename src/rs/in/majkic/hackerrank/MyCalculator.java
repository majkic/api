package rs.in.majkic.hackerrank;

/**
 * Created by majkic on 5/4/16.
 */
class MyCalculator implements AdvancedArithmetic {
    public int divisorSum (int n){
        int sum = 1;
        int i = 2;
        while (n >= i){
            if (n % i == 0){
                sum += i;
            }
            i++;
        }
        return sum;
    }
}

interface AdvancedArithmetic{
    public abstract int divisorSum(int n);
}
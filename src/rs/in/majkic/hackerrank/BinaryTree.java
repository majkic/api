package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/24/16.
 */
public class BinaryTree {

    public static int getHeight(Node root){
        //Write your code here
        if (root.left != null){
            if (root.right != null){
                if (getHeight(root.left) > getHeight(root.right)){
                    return (getHeight(root.left) + 1);
                } else {
                    return (getHeight(root.right) + 1);
                }
            } else {
                return (getHeight(root.left) + 1);
            }
        } else if (root.right != null){
            return (getHeight(root.right) + 1);
        } else {
            return 0;
        }
    }

    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }
        int height=getHeight(root);
        System.out.println(height);
    }
}

class Node{
    Node left,right;
    int data;
    Node(int data){
        this.data=data;
        left=right=null;
    }
}

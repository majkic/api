package rs.in.majkic.hackerrank;

import java.util.Scanner;

/**
 * Created by majkic on 5/12/16.
 */
public class BinarySearch {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int v = sc.nextInt();
        sc.nextLine();
        int n = sc.nextInt();
        sc.nextLine();
        int []arr = new int[n];
        int i;
        for (i=0;i<n;i++){
            arr[i] = sc.nextInt();
        }
        int minindex = 0;
        int maxindex = n;
        int newindex=0;
        while (minindex<maxindex){
            newindex = minindex + (maxindex-minindex)/2;
            if (arr[newindex]>v){
                maxindex = newindex;
            } else if (arr[newindex]<v){
                minindex = newindex;
            } else {
                minindex=maxindex;
            }
        }
        System.out.println(newindex);
    }
}

package rs.in.majkic.hackerrank;

import java.util.*;

/**
 * Created by majkic on 5/24/16.
 */
public class NonDivisibleSubsets {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        sc.nextLine();
        int[] a = new int[n];
        for (int i=0;i<n;i++){
            a[i] = sc.nextInt();
        }
        List<Pair<Integer, Integer>> pairs = new ArrayList<>();
        for (int i=0;i<n-1;i++){
            for (int j=i+1;j<n;j++){
                pairs.add(new Pair(a[i], a[j]));
            }
        }
        pairs = findNonDivisiblePairs(pairs, k);
        Set<Integer> s = createSet(pairs);
        int count = s.size();
        System.out.println(count);
    }

    private static Set<Integer> createSet(List<Pair<Integer, Integer>> pairs) {
        Set<Integer> set = new HashSet<>();
        for (Pair<Integer, Integer> pair:pairs){
            if (!set.contains(pair.first)){
                set.add(pair.first);
            }
            if (!set.contains(pair.second)){
                set.add(pair.second);
            }
        }
        return set;
    }

    private static List<Pair<Integer, Integer>> findNonDivisiblePairs(List<Pair<Integer, Integer>> list, int k) {
        List<Pair<Integer, Integer>> newList = new ArrayList<Pair<Integer, Integer>>();
        for (Pair<Integer, Integer> pair:list) {
            if (!((pair.first + pair.second) % k == 0)){
                newList.add(pair);
            }
        }
        return newList;
    }

}

class Pair<L, R>{
        L first;
        R second;
public Pair(L first, R second){
        this.first = first;
        this.second = second;
        }
}

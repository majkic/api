package rs.in.majkic.hackerrank;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by majkic on 7/3/16.
 */
public class LonelyInteger {
    private static int lonelyInteger(int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (!arrayContains(a[i], a)) {
                return a[i];
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }

        System.out.println(lonelyInteger(a));
    }

    public static boolean arrayContains(int num, int[] a){
        int count = 0;
        for (int i=0;i<a.length;i++){
            if (a[i] == num) {
                count++;
            }
        }
        if (count == 1) {
            return false;
        }
        return true;
    }

}

package rs.in.majkic.codility.arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/4/17.
 */
public class CyclicRotationTest {
    private CyclicRotation cyclicRotation;

    @Before
    public void setUp() throws Exception {
        cyclicRotation = new CyclicRotation();
    }

    @After
    public void tearDown() throws Exception {
        cyclicRotation = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertArrayEquals(new int[]{3, 4, 1, 2}, cyclicRotation.solution(new int[]{1, 2, 3, 4}, 2));
        assertArrayEquals(new int[]{4, 1, 2, 3}, cyclicRotation.solution(new int[]{1, 2, 3, 4}, 5));
        assertArrayEquals(new int[]{1, 2, 3, 4}, cyclicRotation.solution(new int[]{1, 2, 3, 4}, 100));
        assertArrayEquals(new int[]{}, cyclicRotation.solution(new int[]{}, 100));
    }
}
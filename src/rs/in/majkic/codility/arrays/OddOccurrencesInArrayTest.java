package rs.in.majkic.codility.arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/4/17.
 */
public class OddOccurrencesInArrayTest {

    private OddOccurrencesInArray oddOccurrencesInArray;

    @Before
    public void setUp() throws Exception {
        oddOccurrencesInArray = new OddOccurrencesInArray();
    }

    @After
    public void tearDown() throws Exception {
        oddOccurrencesInArray = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(7, oddOccurrencesInArray.solution(new int[] {9,3,9,3,9,7,9}));
        assertEquals(2, oddOccurrencesInArray.solution(new int[] {2,0,0,0,2,1,0,1,2}));
        assertEquals(0, oddOccurrencesInArray.solution(new int[] {2,0,0,0,2,1,0,1,2,2,1,0,1}));
    }
}
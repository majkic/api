package rs.in.majkic.codility.leader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/11/17.
 */
public class DominatorTest {

    private Dominator dominator;

    @Before
    public void setUp() throws Exception {
        dominator = new Dominator();
    }

    @After
    public void tearDown() throws Exception {
        dominator = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(0, dominator.solution(new int[]{3, 4, 3, 2, 3, -1, 3, 3}));
    }
}
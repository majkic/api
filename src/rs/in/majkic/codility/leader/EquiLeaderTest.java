package rs.in.majkic.codility.leader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/11/17.
 */
public class EquiLeaderTest {

    private EquiLeader equiLeader;

    @Before
    public void setUp() throws Exception {
        equiLeader = new EquiLeader();
    }

    @After
    public void tearDown() throws Exception {
        equiLeader = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(2, equiLeader.solution(new int[]{4, 3, 4, 4, 4, 2}));
    }
}
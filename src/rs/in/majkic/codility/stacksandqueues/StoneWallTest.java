package rs.in.majkic.codility.stacksandqueues;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/9/17.
 */
public class StoneWallTest {

    private StoneWall stoneWall;

    @Before
    public void setUp() throws Exception {
        stoneWall = new StoneWall();
    }

    @After
    public void tearDown() throws Exception {
        stoneWall = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(7, stoneWall.solution(new int[] {8, 8, 5, 7, 9, 8, 7, 4, 8}));
        assertEquals(6, stoneWall.solution(new int[] {3, 2, 4, 5, 6, 4, 5, 4, 2}));
    }
}
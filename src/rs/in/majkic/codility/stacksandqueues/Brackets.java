package rs.in.majkic.codility.stacksandqueues;

import java.util.Stack;

/**
 * Created by majkic on 10/10/17.
 * A string S consisting of N characters is considered to be properly nested if any of the following conditions is true:

 S is empty;
 S has the form "(U)" or "[U]" or "{U}" where U is a properly nested string;
 S has the form "VW" where V and W are properly nested strings.
 For example, the string "{[()()]}" is properly nested but "([)()]" is not.

 Write a function:

 class Solution { public int solution(String S); }
 that, given a string S consisting of N characters, returns 1 if S is properly nested and 0 otherwise.

 For example, given S = "{[()()]}", the function should return 1 and given S = "([)()]", the function should return 0, as explained above.

 Assume that:

 N is an integer within the range [0..200,000];
 string S consists only of the following characters: "(", "{", "[", "]", "}" and/or ")".
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N) (not counting the storage required for input arguments).
 */
public class Brackets {
    public int solution(String S){
        if (S.length() == 0){
            return 1;
        }
        char[] chars = S.toCharArray();
        int response = 1;
        Stack stack = new Stack();
        stack.push(chars[0]);
        for (int i=1;i<chars.length;i++){
            if (stack.empty() || (opposite((char) stack.peek()) != chars[i])){
                stack.push(chars[i]);
            } else {
                stack.pop();
            }
        }
        if (stack.size()!= 0){
            response = 0;
        }
        return response;
    }

    private char opposite(char a){
        char r;
        switch (a){
            case '{':
                r = '}';
                break;
            case '[':
                r = ']';
                break;
            case '(':
                r = ')';
                break;
            case '}':
                r = '{';
                break;
            case ']':
                r = '[';
                break;
            case ')':
                r = '(';
                break;
            default:
                r = '=';
        }
        return r;
    }
}

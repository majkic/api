package rs.in.majkic.codility.stacksandqueues;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by majkic on 10/9/17.
 * You are going to build a stone wall. The wall should be straight and N meters long, and its thickness should be constant; however, it should have different heights in different places. The height of the wall is specified by a zero-indexed array H of N positive integers. H[I] is the height of the wall from I to I+1 meters to the right of its left end. In particular, H[0] is the height of the wall's left end and H[N−1] is the height of the wall's right end.

 The wall should be built of cuboid stone blocks (that is, all sides of such blocks are rectangular). Your task is to compute the minimum number of blocks needed to build the wall.

 Write a function:

 int solution(int H[], int N);
 that, given a zero-indexed array H of N positive integers specifying the height of the wall, returns the minimum number of blocks needed to build it.

 For example, given array H containing N = 9 integers:

 H[0] = 8    H[1] = 8    H[2] = 5
 H[3] = 7    H[4] = 9    H[5] = 8
 H[6] = 7    H[7] = 4    H[8] = 8
 the function should return 7. The figure shows one possible arrangement of seven blocks.



 Assume that:

 N is an integer within the range [1..100,000];
 each element of array H is an integer within the range [1..1,000,000,000].
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 */
public class StoneWall {
    public int solution(int H[]){
        int counter = 0;

        Stack stack = new Stack();
        stack.push(H[0]);
        counter++;
        for (int i=1;i<H.length;i++){
            // If the height of the wall grows we have to use another block
            if (H[i] > (int)stack.peek()){
                stack.push(H[i]);
                counter ++;
            // If the height of the wall decreases we have to check is there is a block with the same size as the required
            } else if (H[i] < (int) stack.peek()){
                // If H[k] is smaller than the current head keep looking
                if (H[i] < (int)stack.peek()){
                    while (H[i] < (int)stack.peek()){
                        stack.pop();
                        //If the stack is empty we have to use a new block
                        if (stack.size() == 0){
                            stack.push(H[i]);
                            counter++;
                            break;
                        }
                        // If H[k] is greater than the head there is no way to use a block so we have to use a new one
                        else if (H[i] > (int)stack.peek()){
                            stack.push(H[i]);
                            counter++;
                            break;
                        }
                    }
                    // If H[k] is greater than the head there is no way to use a block so we have to use a new one
                } else if(H[i] > (int) stack.peek()){
                    stack.push(H[i]);
                    counter++;
                }
            }

        }
        return counter;
    }

    public int notMySolution(int H[]){
        int numberOfBlocks = 0;
        int totalHeight = 0;

        final ArrayList<Integer> activeBlocks = new ArrayList<Integer>(); // depending on this being insertion-order.

        for (int i = 0; i < H.length; i++)
        {
            int height = H[i];

            if (totalHeight > height)
            {
                // remove block(s) - add another if necessary.

                for (int j = activeBlocks.size() - 1; j >= 0; j--) // first trouble - how do we scan and remove lesser blocks in O(N) time... is it still O(N)?
                {
                    int latestBlock = activeBlocks.get(j);
                    activeBlocks.remove(j);
                    totalHeight -= latestBlock;

                    if ((totalHeight) <= height) // removed the latest block and now we're not too high.
                    {
                        // we're done.
                        break;
                    }
                }

                // adding the other if necessary.
                if (totalHeight < height)
                {
                    int newBlock = height - totalHeight;
                    totalHeight += newBlock;
                    activeBlocks.add(newBlock);
                    numberOfBlocks++;
                }
            }
            else if (totalHeight == height)
            {
                continue;
            }
            else
            {
                int newBlock = height - totalHeight;
                activeBlocks.add(newBlock);
                totalHeight += newBlock;

                // add a block. increase total height.
                numberOfBlocks++;
            }
        }

        return numberOfBlocks;
    }
}

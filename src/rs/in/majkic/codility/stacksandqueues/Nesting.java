package rs.in.majkic.codility.stacksandqueues;

import java.util.Stack;

/**
 * Created by majkic on 10/10/17.
 * A string S consisting of N characters is called properly nested if:

 S is empty;
 S has the form "(U)" where U is a properly nested string;
 S has the form "VW" where V and W are properly nested strings.
 For example, string "(()(())())" is properly nested but string "())" isn't.

 Write a function:

 class Solution { public int solution(String S); }
 that, given a string S consisting of N characters, returns 1 if string S is properly nested and 0 otherwise.

 For example, given S = "(()(())())", the function should return 1 and given S = "())", the function should return 0, as explained above.

 Assume that:

 N is an integer within the range [0..1,000,000];
 string S consists only of the characters "(" and/or ")".
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(1) (not counting the storage required for input arguments).
 */
public class Nesting {
    public int solution(String S){
        if (S.length() == 0){
            return 1;
        }
        char[] chars = S.toCharArray();
        if (chars[0] == ')'){
            return 0;
        }
        int response = 1;
        Stack stack = new Stack();
        stack.push(chars[0]);
        for (int i=1;i<chars.length;i++){
            if (stack.empty() && chars[i] == ')'){
                return 0;
            }
            if (stack.empty() || (((char) stack.peek()) == chars[i])) {
                stack.push(chars[i]);
            } else {
                stack.pop();
            }
        }

        if (stack.size()!= 0){
            response = 0;
        }
        return response;
    }
}

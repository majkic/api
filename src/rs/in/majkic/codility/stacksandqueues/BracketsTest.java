package rs.in.majkic.codility.stacksandqueues;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/10/17.
 */
public class BracketsTest {

    private Brackets brackets;

    @Before
    public void setUp() throws Exception {
        brackets = new Brackets();
    }

    @After
    public void tearDown() throws Exception {
        brackets = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(1, brackets.solution("{[()()]}"));
        assertEquals(0, brackets.solution("([)()]"));
        assertEquals(1, brackets.solution("[{}()[]]"));
        assertEquals(1, brackets.solution("{}()"));
        assertEquals(1, brackets.solution("{[]}(){[()]}"));
        assertEquals(1, brackets.solution(""));
        //assertEquals(0, brackets.solution("}{"));
    }
}
package rs.in.majkic.codility.stacksandqueues;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/10/17.
 */
public class FishTest {

    private Fish fish;

    @Before
    public void setUp() throws Exception {
        fish = new Fish();
    }

    @After
    public void tearDown() throws Exception {
        fish = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(2, fish.solution(new int[] {4, 3, 2, 1, 5}, new int[]{0, 1, 0, 0, 0}));
    }
}
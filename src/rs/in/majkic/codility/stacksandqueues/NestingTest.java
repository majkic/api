package rs.in.majkic.codility.stacksandqueues;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/10/17.
 */
public class NestingTest {

    private Nesting nesting;

    @Before
    public void setUp() throws Exception {
        nesting = new Nesting();
    }

    @After
    public void tearDown() throws Exception {
        nesting = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(1, nesting.solution("(())"));
        assertEquals(0, nesting.solution("(()))))"));
        assertEquals(1, nesting.solution("(())()()((()))"));
        assertEquals(0, nesting.solution("(()))"));
        assertEquals(0, nesting.solution("(()))("));
    }
}
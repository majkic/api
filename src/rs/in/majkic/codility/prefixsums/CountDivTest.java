package rs.in.majkic.codility.prefixsums;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class CountDivTest {

    private CountDiv countDiv;

    @Before
    public void setUp() throws Exception {
        countDiv = new CountDiv();
    }

    @After
    public void tearDown() throws Exception {
        countDiv = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(3, countDiv.solution(6, 11, 2));
        assertEquals(4, countDiv.solution(6, 12, 2));
        assertEquals(4, countDiv.solution(6, 13, 2));
        assertEquals(3, countDiv.solution(6, 12, 3));
        assertEquals(8, countDiv.solution(6, 83, 10));
    }
}
package rs.in.majkic.codility.prefixsums;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class PassingCarsTest {

    private PassingCars passingCars;

    @Before
    public void setUp() throws Exception {
        passingCars = new PassingCars();
    }

    @After
    public void tearDown() throws Exception {
        passingCars = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(5, passingCars.solution(new int[] {0, 1, 0, 1, 1}));
        assertEquals(8, passingCars.solution(new int[] {0, 1, 0, 1, 1, 0, 1}));
        assertEquals(3, passingCars.solution(new int[] {1, 0, 1, 0, 1, 1}));
        assertEquals(1, passingCars.solution(new int[] {1, 0}));
        assertEquals(0, passingCars.solution(new int[] {0, 0}));
    }
}
package rs.in.majkic.codility.prefixsums;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class GenomicRangeQueryTest {

    private GenomicRangeQuery genomicRangeQuery;

    @Before
    public void setUp() throws Exception {
        genomicRangeQuery = new GenomicRangeQuery();
    }

    @After
    public void tearDown() throws Exception {
        genomicRangeQuery = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertArrayEquals(new int[] {2, 4, 1}, genomicRangeQuery.solution("CAGCCTA", new int[]{2, 5, 0}, new int[]{4, 5, 6}));
    }
}
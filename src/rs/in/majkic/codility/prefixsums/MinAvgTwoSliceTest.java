package rs.in.majkic.codility.prefixsums;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class MinAvgTwoSliceTest {

    private MinAvgTwoSlice minAvgTwoSlice;

    @Before
    public void setUp() throws Exception {
        minAvgTwoSlice = new MinAvgTwoSlice();
    }

    @After
    public void tearDown() throws Exception {
        minAvgTwoSlice = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(1, minAvgTwoSlice.solution(new int[] {4, 2, 2, 5, 1, 5, 8}));
    }
}
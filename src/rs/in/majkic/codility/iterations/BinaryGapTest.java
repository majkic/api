package rs.in.majkic.codility.iterations;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 9/26/17.
 */
public class BinaryGapTest {

    private BinaryGap binaryGap;

    @Before
    public void setUp() throws Exception {
        binaryGap = new BinaryGap();
    }

    @After
    public void tearDown() throws Exception {
        binaryGap = null;
    }

    @Test
    public void testZeroNumber() throws Exception {
        assertEquals(5, binaryGap.zeroNumber(130));
        assertEquals(1, binaryGap.zeroNumber(2000));
        assertEquals(3, binaryGap.zeroNumber(69));
        assertEquals(0, binaryGap.zeroNumber(1024));
        assertEquals(4, binaryGap.zeroNumber(530));
        assertEquals(2, binaryGap.zeroNumber(72));
        assertEquals(3, binaryGap.zeroNumber(2320));
        assertEquals(5, binaryGap.zeroNumber(1041));
    }
}
package rs.in.majkic.codility.sorting;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class MaxProductOfThreeTest {

    private MaxProductOfThree maxProductOfThree;

    @Before
    public void setUp() throws Exception {
        maxProductOfThree = new MaxProductOfThree();
    }

    @After
    public void tearDown() throws Exception {
        maxProductOfThree = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(60, maxProductOfThree.solution(new int[] {-3, 1, 2, -2, 5, 6}));
    }
}
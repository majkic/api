package rs.in.majkic.codility.sorting;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class TriangleTest {

    private Triangle triangle;

    @Before
    public void setUp() throws Exception {
        triangle = new Triangle();
    }

    @After
    public void tearDown() throws Exception {
        triangle = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(1, triangle.solution(new int[] {10, 2, 5, 1, 8, 20}));
        assertEquals(0, triangle.solution(new int[] {10, 50, 5, 1}));
    }
}
package rs.in.majkic.codility.sorting;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class DistinctTest {

    private Distinct distinct;

    @Before
    public void setUp() throws Exception {
        distinct = new Distinct();
    }

    @After
    public void tearDown() throws Exception {
        distinct = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(3, distinct.solution(new int[] {2, 1, 1, 2, 3, 1}));
    }
}
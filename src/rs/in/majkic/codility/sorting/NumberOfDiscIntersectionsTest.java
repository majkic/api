package rs.in.majkic.codility.sorting;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/8/17.
 */
public class NumberOfDiscIntersectionsTest {

    private NumberOfDiscIntersections numberOfDiscIntersections;

    @Before
    public void setUp() throws Exception {
        numberOfDiscIntersections = new NumberOfDiscIntersections();
    }

    @After
    public void tearDown() throws Exception {
        numberOfDiscIntersections = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(11, numberOfDiscIntersections.solution(new int[] {1, 5, 2, 1, 4, 0}));
    }
}
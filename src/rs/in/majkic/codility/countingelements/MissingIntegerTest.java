package rs.in.majkic.codility.countingelements;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/7/17.
 */
public class MissingIntegerTest {

    private MissingInteger missingInteger;
    @Before
    public void setUp() throws Exception {
        missingInteger = new MissingInteger();
    }

    @After
    public void tearDown() throws Exception {
        missingInteger = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(5, missingInteger.solution(new int[] {1, 3, 6, 4, 1, 2}));
        assertEquals(4, missingInteger.solution(new int[] {1, 2, 3}));
        assertEquals(1, missingInteger.solution(new int[] {-3, -1}));
        assertEquals(5, missingInteger.solution(new int[] {-3, 0, 1, 4, 6, 2, 3}));
    }
}
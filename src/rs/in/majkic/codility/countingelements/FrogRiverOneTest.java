package rs.in.majkic.codility.countingelements;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/6/17.
 */
public class FrogRiverOneTest {
    private FrogRiverOne frogRiverOne;

    @Before
    public void setUp() throws Exception {
        frogRiverOne = new FrogRiverOne();
    }

    @After
    public void tearDown() throws Exception {
        frogRiverOne = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(6, frogRiverOne.solution(5, new int[] {1, 3, 1, 4, 2, 3, 5, 4}));
        assertEquals(7, frogRiverOne.solution(5, new int[] {1, 1, 1, 2, 2, 3, 5, 4}));
    }
}
package rs.in.majkic.codility.countingelements;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/7/17.
 */
public class MaxCountersTest {
    private MaxCounters maxCounters;

    @Before
    public void setUp() throws Exception {
        maxCounters = new MaxCounters();
    }

    @After
    public void tearDown() throws Exception {
        maxCounters = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertArrayEquals(new int[] {3, 2, 2, 4, 2}, maxCounters.solution(5, new int[] {3, 4, 4, 6, 1, 4, 4}));
    }
}
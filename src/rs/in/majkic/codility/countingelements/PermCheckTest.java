package rs.in.majkic.codility.countingelements;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/6/17.
 */
public class PermCheckTest {
    private PermCheck permCheck;
    @Before
    public void setUp() throws Exception {
        permCheck = new PermCheck();
    }

    @After
    public void tearDown() throws Exception {
        permCheck = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(1, permCheck.solution(new int[] {4, 1, 3, 2}));
        assertEquals(0, permCheck.solution(new int[] {4, 1, 3}));
        assertEquals(0, permCheck.solution(new int[] {4, 1, 3, 1}));
        assertEquals(1, permCheck.solution(new int[] {4, 1, 3, 2, 7, 6, 5}));
        assertEquals(0, permCheck.solution(new int[] {4, 3, 2}));
        assertEquals(0, permCheck.solution(new int[] {3, 1, 3, 3}));
    }
}
package rs.in.majkic.codility.countingelements;

/**
 * Created by majkic on 10/7/17.
 * This is a demo task.

 Write a function:

 class Solution { public int solution(int[] A); }
 that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

 For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

 For another example, given A = [1, 2, 3], the function should return 4.

 Given A = [−1, −3], the function should return 1.

 Assume that:

 N is an integer within the range [1..100,000];
 each element of array A is an integer within the range [−1,000,000..1,000,000].
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 */
public class MissingInteger {
    public int solution(int[] A){
        int result = 1;
        boolean[] occurencies = new boolean[100000];
        int i;
        System.out.println("Occ length: " + occurencies.length);
        for (i=0; i<A.length; i++){
            if (A[i] > 0){
                occurencies[A[i]] = true;
            }
        }
        for (i=1; i<occurencies.length; i++){
            if (occurencies[i] == false){
                if (i<100){
                    System.out.println("occ" + i + " = "+ occurencies[i]);
                }
                return i;
            }
        }
        return 1;
    }
}

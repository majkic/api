package rs.in.majkic.codility.timecomplexity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/5/17.
 */
public class PermMissingElemTest {
    private PermMissingElem permMissingElem;
    @Before
    public void setUp() throws Exception {
        permMissingElem = new PermMissingElem();
    }

    @After
    public void tearDown() throws Exception {
        permMissingElem = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(3, permMissingElem.solution(new int[] {1,5,4,2}));
        assertEquals(4, permMissingElem.solution(new int[] {2,3,1,5}));
    }
}
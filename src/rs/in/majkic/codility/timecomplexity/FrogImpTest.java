package rs.in.majkic.codility.timecomplexity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/5/17.
 */
public class FrogImpTest {
    private FrogImp frogImp;

    @Before
    public void setUp() throws Exception {
        frogImp = new FrogImp();
    }

    @After
    public void tearDown() throws Exception {
        frogImp = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(3, frogImp.solution(10, 85, 30));
        assertEquals(2, frogImp.solution(10, 70, 30));
        assertEquals(333334, frogImp.solution(10000000, 20000000, 30));
    }
}
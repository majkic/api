package rs.in.majkic.codility.timecomplexity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 10/6/17.
 */
public class TapeEquilibriumTest {
    private TapeEquilibrium tapeEquilibrium;

    @Before
    public void setUp() throws Exception {
        tapeEquilibrium = new TapeEquilibrium();
    }

    @After
    public void tearDown() throws Exception {
        tapeEquilibrium = null;
    }

    @Test
    public void testSolution() throws Exception {
        assertEquals(1, tapeEquilibrium.solution(new int[]{3, 1, 2, 4, 3}));
        assertEquals(2, tapeEquilibrium.solution(new int[]{3, 1, 2, 4, 3, 8, 7}));
    }
}
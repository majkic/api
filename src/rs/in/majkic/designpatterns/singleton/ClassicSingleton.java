package rs.in.majkic.designpatterns.singleton;

/**
 * Created by majkic on 4/27/16.
 */
public class ClassicSingleton {
    private static ClassicSingleton instance = null;
    /*
    Needs to be private (was protected) to prevent calling constructor.
    The only way to instantiate the object is through getInstance() method.
    */
    private ClassicSingleton() {
        // Exists only to defeat instantiation.
    }
    public static ClassicSingleton getInstance() {
        if(instance == null) {
            instance = new ClassicSingleton();
        }
        return instance;
    }
}

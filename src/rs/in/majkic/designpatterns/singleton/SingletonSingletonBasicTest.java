package rs.in.majkic.designpatterns.singleton;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by majkic on 4/27/16.
 */
public class SingletonSingletonBasicTest {
    @Test
    public void constructTest(){
        SingletonBasic b = SingletonBasic.getInstance();
        b.setField(1);
        assertEquals(b.getField(), 1);
        SingletonBasic newB = SingletonBasic.getInstance();
        assertEquals(newB.getField(), 1);
        assertEquals(newB, b);
    }

    @Test
    public void classicSingletonTest(){
        ClassicSingleton cs = ClassicSingleton.getInstance();
        ClassicSingleton cs1 = ClassicSingleton.getInstance();
        ClassicSingleton cs2 = ClassicSingleton.getInstance();
        assertEquals(cs, cs1);
        assertEquals(cs, cs2);
    }
}

package rs.in.majkic.designpatterns.singleton;

/**
 * Created by majkic on 4/27/16.
 */
public class SingletonBasic {
    private int field = 0;
    private static SingletonBasic ourInstance = new SingletonBasic();

    public static SingletonBasic getInstance() {
        return ourInstance;
    }

    private SingletonBasic() {
    }

    public void setField(int i){
        this.field = i;
    }

    public int getField(){
        return this.field;
    }
}

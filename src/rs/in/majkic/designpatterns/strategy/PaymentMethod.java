package rs.in.majkic.designpatterns.strategy;

/**
 * Created by majkic on 11/16/17.
 */
public interface PaymentMethod {
    public void pay(int cents);
}

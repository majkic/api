package rs.in.majkic.designpatterns.strategy;

/**
 * Created by majkic on 11/16/17.
 */
public class DebitCard extends Card {
    public DebitCard(String nameOnCard, String number, String cvv, String expirationDate) {
        super(nameOnCard, number, cvv, expirationDate);
    }

    protected String getType(){
        return "debit";
    }

    protected void executeTransaction(int cents){
        // contact credit holder to make the transaction
    }
}

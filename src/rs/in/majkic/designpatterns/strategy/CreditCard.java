package rs.in.majkic.designpatterns.strategy;

/**
 * Created by majkic on 11/16/17.
 */
public class CreditCard extends Card {
    public CreditCard(String nameOnCard, String number, String cvv, String expirationDate) {
        super(nameOnCard, number, cvv, expirationDate);
    }

    protected String getType(){
        return "credit";
    }

    protected void executeTransaction(int cents){
        // contact credit holder to make the transaction
    }
}

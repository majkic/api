package rs.in.majkic.designpatterns.strategy;

/**
 * Created by majkic on 11/16/17.
 */
public class PaymentMethodFactory {
    public static PaymentMethod getPaymentMethod (String type){
            switch (type){
                case "credit":
                    return createCreditCard();
                case "debit":
                    return createDebitCard();
                case "cash":
                    return createCash();
            }
        throw new IllegalArgumentException();
    }

    public static Card createCreditCard(){
        return new CreditCard("John Doe", "11111111", "1234", "08/22");
    }

    public static Card createDebitCard(){
        return new DebitCard("John Doe", "11111111", "1234", "08/22");
    }

    public static Cash createCash(){
        return new Cash();
    }
}

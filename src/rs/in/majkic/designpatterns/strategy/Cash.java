package rs.in.majkic.designpatterns.strategy;

/**
 * Created by majkic on 11/16/17.
 */
public class Cash implements PaymentMethod {
    @Override
    public void pay(int cents) {
        System.out.println("Paid in cash: " + cents + " cents.");
    }
}

package rs.in.majkic.designpatterns.factory;

import java.util.logging.Logger;

/**
 * Created by majkic on 4/28/16.
 */
public class Circle implements Shape {
    private static final Logger LOGGER = Logger.getLogger(Circle.class.getName());

    public void draw() {
        LOGGER.info("Circle draw method");
    }
}

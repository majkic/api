package rs.in.majkic.designpatterns.factory;

import java.util.logging.Logger;

/**
 * Created by majkic on 4/28/16.
 */
public class Rectangle implements Shape {
    private static final Logger LOGGER = Logger.getLogger(Rectangle.class.getName());

    public void draw() {
        LOGGER.info("Rectangle draw method");
    }
}

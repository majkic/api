package rs.in.majkic.designpatterns.factory;

import java.util.logging.Logger;

/**
 * Created by majkic on 4/28/16.
 */
public class Basic {
    private static final Logger logger = Logger.getLogger(Basic.class.getName());
    public static void main (String[] args){
        Shape shape1 = (new ShapeFactory()).getShape("CIRCLE");
        shape1.draw();
        Shape shape2 = (new ShapeFactory()).getShape("SQUARE");
        if (shape2 != null) shape2.draw();
        Shape shape3 = (new ShapeFactory()).getShape("RECTANGLE");
        if (shape3 != null) shape3.draw();
    }
}

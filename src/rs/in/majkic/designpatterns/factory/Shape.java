package rs.in.majkic.designpatterns.factory;

/**
 * Created by majkic on 4/28/16.
 */
public interface Shape {
    public void draw();
}

package rs.in.majkic.designpatterns.factory;

import java.util.logging.Logger;

/**
 * Created by majkic on 4/28/16.
 */
public class Square implements Shape {
    private static final Logger LOGGER = Logger.getLogger(Square.class.getName());
    @Override
    public void draw() {
        LOGGER.info("Square draw method");
    }
}

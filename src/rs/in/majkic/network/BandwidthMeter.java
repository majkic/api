package rs.in.majkic.network;

import java.io.*;
import java.net.*;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Created by majkic on 5/19/16.
 */
public class BandwidthMeter {

    private static String URL = "http://majkic.in.rs/metaphors.pdf";
    private static String DESTINATION = "/tmp/metaphors.pdf";

    public static void main(String[] args){
        BandwidthMeter bm = new BandwidthMeter();
        System.out.println("Bandwidth in [kbps]" + bm.getBandwidth());
    }

    public double getBandwidth(){
        double bw = 0.0;
        try {
            long time = getTime()/1000000;
            long size = getSize()*8;
            bw =  size * 1.0 / time;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bw;
    }

    public long getTime() throws IOException {
        long loopStartTime = System.nanoTime();
        URL website = new URL(URL);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(DESTINATION);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        long loopEndTime = System.nanoTime();
        long time = loopEndTime - loopStartTime;
        return time;
    }

    public long getSize() {
        File me = new File(DESTINATION);
        return me.length();
    }

}

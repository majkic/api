package rs.in.majkic.api.lambda;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majkic on 1/25/18.
 */
public class LambdaTest {

    private Lambda lambda;
    @Before
    public void setUp() throws Exception {
        lambda = new Lambda();
    }

    @After
    public void tearDown() throws Exception {
        lambda = null;
    }

    @Test
    public void testInc() throws Exception {
        assertEquals(lambda.inc(5, 6), 11);
    }

    @Test
    public void testMessage() throws Exception {
        assertEquals(lambda.message("Marko"), "Hello Marko");
    }
}
package rs.in.majkic.api.lambda;

import java.awt.*;

/**
 * Created by majkic on 1/23/18.
 */
public class Lambda {
    public static void main(String[] args) {


        Frame frame=new Frame("ActionListener java8");

        Button b=new Button("Click Here");
        b.setBounds(50,100,80,50);
        /*
        Lambda is setting action listener for action for Button b
         */
        Lambda l = new Lambda();
        b.addActionListener(e -> System.out.println(l.message("Marko")));
        frame.add(b);

        frame.setSize(200,200);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public void lambdaCall(String s){
        LambdaFunction lambdaFunction = () -> System.out.println("Lambda executed");
        lambdaFunction.call();
    }

    public String message(String name){
        MyFunctionalInterface msg = (s) -> {
            return "Hello " + s;
        };
        return(msg.sayHello(name));
    }

    public int inc(int a, int inc){
        // lambda expression with single parameter num
        AnotherFunctionalInterface f = (num) -> num + inc;
        return f.incrementByX(a);
    }


    // Interfaces
    @FunctionalInterface
    interface MyFunctionalInterface {

        public String sayHello(String s);
    }

    @FunctionalInterface
    interface AnotherFunctionalInterface{
        public int incrementByX(int a);
    }

    @FunctionalInterface
    interface LambdaFunction{
        void call();
    }
}

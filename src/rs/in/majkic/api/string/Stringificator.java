package rs.in.majkic.api.string;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by majkic on 6/16/16.
 */
public class Stringificator {
    /**
     * This method is reverting a given string
     * @param s
     * @return
     */
    public String reverseString(String s){
        String finalString = "";
        for (int i = s.length()-1; i>=0; i--){
            finalString += s.substring(i,i+1);
        }
        return finalString;
    }

    public String findTagInString(String input){
        // First find start of tag
        int positionStart = input.indexOf("<") + 1;
        int positionEnd = input.indexOf(">");
        String sub = input.substring(positionStart, positionEnd);
        //Then find tag end
        int positionTagEndStart = input.indexOf("</" + sub + ">") - 1;
        int positionTagEndEnd = positionTagEndStart + 3 + sub.length() - 1;
        String newSub = input.substring(positionStart - 1, positionTagEndEnd + 2);

        return newSub;
    }

    public String intToString(int i){
        return String.valueOf(i);
    }

    public int stringToInt(String a){
        return Integer.parseInt(a);
    }

    /* Deprecated
    public Date stringToDate(String a, String dateFormat){
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        Date date = null;
        try {
            date = format.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    */

    public Calendar stringToDate(String a, String dateFormat){
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(format.parse(a));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return c;
    }
}

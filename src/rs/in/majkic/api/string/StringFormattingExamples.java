package rs.in.majkic.api.string;

import java.util.Formatter;

/**
 * Created by majkic on 3/27/17.
 * as found on https://dzone.com/articles/java-string-format-examples
 */
public class StringFormattingExamples {

    public static void main (String[] args){
        String output = String.format("%s = %d", "joe", 35);
        System.out.println(output);

        System.out.printf("My name is: %s%n", "joe");

        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        fmt.format("PI = %f%n", Math.PI);
        System.out.println(sbuf.toString());
        // you can continue to append data to sbuf here.

        System.out.println(String.format("%2$s", 32, "Hello")); // prints: "Hello"

        System.out.println(String.format("%d", 93));
        System.out.println(String.format("|%20d|", 93)); // prints: |                  93|
        System.out.println(String.format("|%-20d|", 93)); // prints: |93                  |);
        System.out.println(String.format("|%020d|", 93)); // prints: |00000000000000000093|);
        System.out.println(String.format("|%+20d|", 93)); // prints: |                 +93|);
        System.out.println(String.format("|% d|", 93)); // prints: | 93| String.format("|% d|", -36); // prints: |-36|);
        System.out.println(String.format("|%,d|", 10000000)); // prints: |10,000,000|);
        System.out.println(String.format("|%(d|", -36)); // prints: |(36)|);
        System.out.println(String.format("|%o|", 93)); // prints: 135); // octal output
        System.out.println(String.format("|%#o|", 93));// prints: 0135);
        System.out.println(String.format("|%#x|", 93));// prints: 0x5d);
        System.out.println(String.format("|%#X|", 93));// prints: 0X5D);
        System.out.println(String.format("|%s|", "Hello World")); // prints: "Hello World"
        System.out.println(String.format("|%-30s|", "Hello World")); // prints: |Hello World |
        System.out.println(String.format("|%.5s|", "Hello World")); // prints: |Hello|
        System.out.println(String.format("|%30.5s|", "Hello World")); //| Hello|
    }

}

package rs.in.majkic.api.string;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by majkic on 4/27/16.
 */
public class StringificatorTest {
    public Stringificator stringificator;
    @Before
    public void setUp() throws Exception {
        stringificator = new Stringificator();
    }

    @After
    public void tearDown() throws Exception {
        stringificator = null;
    }
    @Test
    public void reverseStringTest(){
        assertEquals (stringificator.reverseString("Marko"), "okraM");
        assertEquals (stringificator.reverseString("anavolimilovana"), "anavolimilovana");
        assertEquals (stringificator.reverseString("zika"), "akiz");
    }

    @Test
    public void findTagInStringTest(){
        assertEquals ("<blabla>ovo je ok</blabla>", stringificator.findTagInString("<blabla>ovo je ok</blabla>"));
        assertEquals ("<blabla><a>ovo je ok</a></blabla>", stringificator.findTagInString("<blabla><a>ovo je ok</a></blabla>"));
    }

    @Test
    public void intToString(){
        assertEquals("2", stringificator.intToString(2));
        assertEquals("-2", stringificator.intToString(-2));
    }

    @Test
    public void stringToInt(){
        assertEquals(2, stringificator.stringToInt("2"));
        assertEquals(-2, stringificator.stringToInt("-2"));
    }

    @Test
    public void stringToDate(){
        System.out.println(getCalendar(2016,0,1).getTime());
        System.out.println(stringificator.stringToDate("2016-01-01", "yyyy-MM-dd").getTime());
        assertEquals(getCalendar(2016,0,1).getTime(), stringificator.stringToDate("2016-01-01", "yyyy-mm-dd").getTime());
    }

    private Calendar getCalendar(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 1);//weird enough
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }
}

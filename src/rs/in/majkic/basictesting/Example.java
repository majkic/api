package rs.in.majkic.basictesting;

import java.util.*;

/**
 * Created by majkic on 10/20/17.
 */
public class Example {
    /**
     * Find the factorial of number x!
     Explanation
     n! means n * (n – 1) * ... * 3 * 2 * 1
     For example, 10! = 10 * 9 * ... * 3 * 2 * 1 = 3628800
     * @param n
     * @return
     */
    public int factorial(int n){
        if (n<0) return 0;
        while (n > 1){
            return n*factorial(n-1);
        }
        return 1;
    }

    /**
     * Given an array of integers, find two numbers such that they add up to a specific target number. There is only one pair with such properties.
     Explanation
     Input: numbers=[1, 2, 6, 8, 11, 15], target=13 Output: [2, 11]
     * @param a
     * @return
     */
    public int[] targetSum(int[] a, int target){
        /* Complexity O(n) */
        int[] r = new int[2];
        boolean[] map = new boolean[1000];//max_size = 1000;
        int temp = 0;
        for (int i=0;i<a.length;i++){
            temp = target - a[i];
            if (temp>=0 && map[temp]){
                r[0] = temp;
                r[1] = a[i];
                return r;
            }
            map[a[i]] = true;
        }
        return r;
        /* O(n^2) complexity
        int[] r = new int[2];
        for (int i=0;i<a.length-1;i++){
            for (int j=i+1;j<a.length;j++){
                if (a[i]+a[j] == target){
                    r[0] = a[i];
                    r[1] = a[j];
                    return r;
                }
            }
        }
        return r;*/
    }

    /**
     * Given an input string, reverse the string word by word.
     Explanation
     For example, given “the sky is blue”, return “blue is sky the”
     * @param s
     * @return
     */
    public String reverseString(String s){
        /* Piece of cake using split function in Java
        String[] p = s.split(" ");
        String r = "";
        for (int i=p.length-1;i>=0;i--){
            r += p[i];
            if (i != 0){
                r += " ";
            }
        }
        return r; */
        // Now no extra tools, geeky way
        int l = s.length();
        char[] c = s.toCharArray();
        String sb = "";
        String r = "";
        for (int i=0;i<l;i++){
            if (c[i] != ' '){
                sb += c[i];
            } else {
                r = sb + r;

                if (i != l-1){
                    r = " " + r;
                }
                sb = "";
            }
            if (i == l-1){
                r = sb + r;
            }
        }
        return r;
    }

    /**
     * Given an array of integers, find and print all distinct numbers.
     Explanation
     Input: numbers=[1, 3, 5, 5, 7, 8, 9, 9, 13] Output: [1, 3, 5, 7, 8, 9, 13]
     * @param a
     * @return
     */
    public List<Integer> distinctNumbers(int[] a){
        /* Java specific way, not smart, nor optimal
        Set<Integer> s = new HashSet();
        for (int j=0;j<a.length;j++){
            s.add(a[j]);
        }
        int[] b = new int[s.size()];
        Object[] c = s.toArray();
        for (int i=0;i<s.size();i++){
            Integer temp = (Integer) c[i];
            b[i] = temp.intValue();
        }
        return b;
        */
        //Now optimal, generic way, geeky
        HashSet<Integer> h = new HashSet<>();
        List<Integer> b = new ArrayList<>();
        for (int i=0;i<a.length;i++){
            if (!h.contains(a[i])){
                h.add(Integer.valueOf(a[i]));
                b.add(Integer.valueOf(a[i]));
            }
        }
        return b;
    }

    /**
     * Given an array of integers, for each integer multiply all other integers except that one and return an array containing sums.
     Explanation
     Input: numbers=[3, 5, 6, 8]
     Output: [240, 144, 120, 90]
     How to calculate: [5 * 6 * 8, 3 * 6 * 8, 3 * 5 * 8, 3 * 5 * 6]
     * @param a
     * @return
     */
    public int[] multiplyOthers(int[] a){
        int multiply = 1;
        int countZeroes = 0;
        for (int i=0;i<a.length;i++){
            if (a[i] != 0){
                multiply = multiply * a[i];
            } else {
                countZeroes++;
            }
        }
        int[] b = new int[a.length];
        for (int i=0;i<a.length;i++){
            if (a[i] != 0) {
                if (countZeroes == 0){
                    b[i] = multiply / a[i];
                } else {
                    b[i] = 0;
                }

            } else {
                if (countZeroes > 1){
                    b[i] = 0;
                } else {
                    b[i] = multiply;
                }
            }
        }
        return b;
    }

    /**
     * Given a string, count the occurrence of individual letters (lower and uppercase, are counted as one letter) and list top 5 letters used (skip punctation marks).
     Explanation
     Input: “Lorem ipsum dolor sit amet, consectetur adipiscing elit.” Output: [[i = 6], [e = 5], ...]
     * @param s
     * @return
     */
    public HashMap<String, Integer> letterCount(String s){
        char[] c = s.toCharArray();
        HashMap<String, Integer> h = new HashMap<>();
        Integer temp = 0;
        String str;
        for (int i=0;i<c.length;i++){
            str = String.valueOf(c[i]);
            if (!h.containsKey(str)){
                h.put(String.valueOf(str), 1);
            } else {
                temp = h.get(String.valueOf(str));
                h.put(String.valueOf(str), temp+1);
            }
        }
        return h;
    }

    /**
     * Given string - print out its permutations
     * @param prefix
     * @param str
     */
    public void permutation(String prefix, String str) {
        int n = str.length();
        if (n == 0) System.out.println(prefix);
        else {
            for (int i = 0; i < n; i++)
                permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n));
        }
    }
}

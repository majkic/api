package rs.in.majkic.basictesting;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by majkic on 10/20/17.
 */
public class ExampleTest {
    private Example example;
    @Before
    public void setUp() throws Exception {
        example = new Example();
    }

    @After
    public void tearDown() throws Exception {
        example = null;
    }

    @Test
    public void testFactorial() throws Exception {
        assertEquals(3628800, example.factorial(10));
        assertEquals(1, example.factorial(0));
        assertEquals(0, example.factorial(-1));
    }

    @Test
    public void testTargetSum() throws Exception {
        assertArrayEquals(new int[] {2, 11}, example.targetSum(new int[] {1, 2, 6, 8, 11, 15}, 13));
        assertArrayEquals(new int[] {42, 151}, example.targetSum(new int[] {1, 2, 3, 42, 110, 151}, 193));
        assertArrayEquals(new int[] {0, 0}, example.targetSum(new int[] {1, 2, 6, 8, 11, 15}, 131));
    }

    @Test
    public void testReverseString() throws Exception {
        assertEquals("the sky is blue", example.reverseString("blue is sky the"));
        assertEquals("blue", example.reverseString("blue"));
        assertEquals("the. sky is blue", example.reverseString("blue is sky the."));
    }

    @Test
    public void testDistinctNumbers() throws Exception {
        List<Integer> t = new ArrayList<>();
        t.add(1);t.add(3);t.add(5);t.add(7);t.add(8);t.add(9);t.add(13);
        assertEquals(t, example.distinctNumbers(new int[] {1, 3, 5, 5, 7, 8, 9, 9, 13}));
    }

    @Test
    public void testMultiplyOthers() throws Exception {
        assertArrayEquals(new int[] {240, 144, 120, 90}, example.multiplyOthers(new int[] {3, 5, 6, 8}));
        assertArrayEquals(new int[] {0, 0, 0, 90}, example.multiplyOthers(new int[] {3, 5, 6, 0}));
        assertArrayEquals(new int[] {0, 0, 0, 0}, example.multiplyOthers(new int[] {3, 0, 6, 0}));
    }

    @Test
    public void testLetterCount() throws Exception {
        Integer t = (Integer) example.letterCount("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").get("i");
        assertEquals(Integer.valueOf(6), t);
        t = (Integer) example.letterCount("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").get("e");
        assertEquals(Integer.valueOf(5), t);
        t = (Integer) example.letterCount("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").get(" ");
        assertEquals(Integer.valueOf(7), t);
    }

    @Test
    public void testPermutations() throws Exception {
        example.permutation("", "aabc");
    }

}